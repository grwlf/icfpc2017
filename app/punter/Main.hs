{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NondecreasingIndentation #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}
module Main where

import Network.Simple.TCP
import System.IO
import System.Environment
import Control.Monad
import Control.Concurrent
import Data.ByteString.Lazy (fromStrict,toChunks)
import System.Exit
import qualified Data.ByteString.Char8 as ByteString
import qualified Data.Aeson as Aeson

import Imports
import Types
import Client
import qualified Punter1 as Punter1
import qualified Punter2 as Punter2

onlineMany :: String -> Int -> IO ()
onlineMany port nthreads = do
  forM_ [1..nthreads] $ \i -> do
    forkIO $ do
      threadDelay (1*(10^6 :: Int) :: Int)
      _ <- online port silentClientOptions (\pid _ _ -> return pid) passbot
      return ()

watchdog sig timeout_sec = do
  _ <- forkIO $ do
    flip evalStateT False $ do
      forM_ [0..timeout_sec-1] $ \_ -> do
        liftIO $ threadDelay (1*(10^6 :: Int) :: Int)
        res <- liftIO $ tryTakeMVar sig
        when (res == Just ()) $ do
          put True

      good <- get
      when (not good) $ do
        liftIO $ hPutStrLn stderr "Terminating due to watchdog"
        liftIO $ exitWith (ExitFailure 2)
      liftIO $ hPutStrLn stderr "Watchdog canceled"
  return ()


main :: IO ()
main = do

  sig <- newEmptyMVar

  args <- getArgs
  res <- do
    case args of
      [] -> do
        offline Punter2.punter_gen Punter2.punter

      ("1":port:ndummies:_) -> do
        watchdog sig 5

        onlineMany port (read ndummies)
        online port (defaultClientOptions (Just sig)) Punter1.punter1_gen Punter1.punter1

      ("2":port:ndummies:_) -> do
        watchdog sig 5

        onlineMany port (read ndummies)
        online port (defaultClientOptions (Just sig)) Punter2.punter_gen Punter2.punter

      _ -> do
        hPutStrLn stderr "online mode: <PUNTER_ID> <PORT> <NUM_DUMMIES>"
        hPutStrLn stderr "offline mode: no arguments"
        return Nothing

  case res of
    Just scores ->
      exitSuccess
    Nothing -> do
      exitWith (ExitFailure 1)

