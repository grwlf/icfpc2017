{-# OPTIONS_GHC -Wall #-}

module Main where

import qualified Data.ByteString.Lazy as BS
import qualified Data.Aeson as JSON
import System.Environment (getArgs)

import Types

main :: IO ()
main = do
  file    <- head <$> getArgs
  strJson <- BS.readFile file
  let (Just mapJson) = JSON.decode strJson :: Maybe SiteMap
  print mapJson
