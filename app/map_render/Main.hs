{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

import qualified Data.HashMap.Strict as H
import qualified Data.Vector as V

import System.Environment(getArgs)

import qualified Data.ByteString as BS
import Data.Aeson
import Data.List

import Types
import Render

main = do
  args <- getArgs
  case args of
    (file:_) -> do

      map_data <- BS.readFile file
      case eitherDecodeStrict map_data of
        Right (sm :: SiteMap) -> do
          render sm
        Left err -> do
          print err

    _ -> do
      renderLastSession

