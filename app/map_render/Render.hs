{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeSynonymInstances,FlexibleInstances #-}

module Render where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Graphics.Gloss.Interface.IO.Game

import qualified Data.HashMap.Strict as H
import qualified Data.HashMap.Strict as M
import qualified Data.Vector as V

import System.Environment(getArgs)
import Control.Exception (SomeException(..),catch)

import qualified Data.ByteString as BS
import Data.Aeson
import Data.List

import Types
import Imports hiding(Char)

map_river :: RiverId -> SiteMap -> River
map_river rid SiteMap{..}
  | r_id rid <= V.length map_rivers = map_rivers V.! (r_id rid)
  | otherwise = error "map_river: Vector idx out of range"


site_radius = 1
mine_radius = 4
river_thickness = 1
canvas_size = 1000
scale_factor = 1.8

data Config = Config {
  c_site_radius :: Float,
  c_mine_radius :: Float,
  c_river_thickness :: Float,
  c_special_punter :: Int
  }

pictureSite :: Float -> Site -> Picture
pictureSite rad s = translate (site_x s) (site_y s) $ circleSolid rad

pscale a (x,y) = (x*a, y*a)

thickline th p1@(x1,y1) p2@(x2,y2) = polygon [p1 + np, p2 + np, p2-np, p1-np]
  where
    dp = p2-p1
    np = pscale (1/l * th) (snd dp, -fst dp)
    l = sqrt $ (fst dp)**2 + (snd dp)**2

pictureRivers :: Float -> SiteMap -> [River] -> Picture
pictureRivers th sm rivers = pictures $ map picRiver $ rivers
  where picRiver (River s t) = let s1 = map_site s sm
                                   s2 = map_site t sm
                               in
                               thickline th ((site_x s1), (site_y s1)) ((site_x s2), (site_y s2))

type ColorNum = Int
type LinesDict = [(ColorNum,[River])]

colors = [
    green
  , blue
  , yellow
  , cyan
  , magenta
  , rose
  , violet
  , azure
  , aquamarine
  , chartreuse
  , orange
  ]

mapClr idx =
  case idx < length colors of
    True -> colors !! idx
    False -> black

pictureSiteMap :: Config -> SiteMap -> LinesDict -> Picture
pictureSiteMap Config{..} sm lines = pictures $ [ sites_pic, rivers_pic, mines_pic ] ++ lines_pic_list
  where mines = map (map_site `flip` sm) $ V.toList $ map_mines sm
        sites = H.elems $ map_sites sm
        mines_pic = color red $ pictures $ map (pictureSite c_mine_radius) mines
        sites_pic = pictures $ map (pictureSite c_site_radius) $ sites
        rivers_pic = pictureRivers c_river_thickness sm $ V.toList (map_rivers sm)
        lines_pic_list = flip map lines $ \(punterid, rivers) ->
          case punterid == c_special_punter of
            True -> color red $ pictureRivers (c_river_thickness * 3) sm rivers
            False -> color (mapClr punterid) $ pictureRivers (c_river_thickness * 2) sm rivers

renderWorld World{..} = do
  let
      lines = foldl f [] w_moves
        where
          f acc (MoveClaim Claim{..}) = (p_id claim_punter, [River claim_source claim_target]):acc
          f acc _ = acc

      sm = w_sitemap
      sites = H.elems $ map_sites sm
      num_sites = H.size $ map_sites sm
      dx = maximum (map site_x sites) - minimum (map site_x sites)
      cx = (sum $ map site_x sites) / fromIntegral num_sites
      cy = (sum $ map site_y sites) / fromIntegral num_sites
      pic_scale = dx / canvas_size * scale_factor
      pic = pictureSiteMap (Config { c_site_radius = site_radius * pic_scale,
                                     c_mine_radius = mine_radius * pic_scale,
                                     c_river_thickness = river_thickness * pic_scale,
                                     c_special_punter = p_id w_punter})  sm lines
      full_pic = scale (1 / pic_scale) ( 1 / pic_scale) $ translate (negate cx) (negate cy) $ pic

  return full_pic


updateMoves w@World{..} dir = do
  catch (
    do let file = w_dir </> (printf "move_%02d_pid%01d" nmoveNext pidNext)
       move <- read <$> readFile file
       putStrLn $ "move #" <> show nmoveNext <> "\n" <> printMove move <> "\n"
       let movesNext = case dir of
                         1    -> move : w_moves
                         (-1) -> if length w_moves > 0
                                 then tail w_moves
                                 else w_moves
                         _    -> w_moves
       return w { w_pid = pidNext, w_nmove = nmoveNext, w_moves = movesNext }
    )
    (\(s::SomeException) -> do
        return w
    )
  where pidNext   = (w_pid + dir) `mod` w_punters
        nmoveNext = w_nmove + (w_pid + dir) `div` w_punters

kbdControl ev w = do
  case ev of
    (EventKey (Char 'j') Down _ _) -> updateMoves w 1
    (EventKey (Char 'k') Down _ _) -> updateMoves w (-1)
    (EventKey (Char 'n') Down _ _) -> iter (w_punters w) (updateMoves `flip` 1) w
    (EventKey (Char 'm') Down _ _) -> iter (w_punters w) (updateMoves `flip` (-1)) w
    (EventKey (Char 'u') Down _ _) -> iter (w_punters w * 5) (updateMoves `flip` 1) w
    (EventKey (Char 'i') Down _ _) -> iter (w_punters w * 5) (updateMoves `flip` (-1)) w
    _ -> return w
  where iter 0 act w = return w
        iter n act w = do
          w' <- act w
          iter (n-1) act w'

data World = World { w_sitemap :: SiteMap
                   , w_punters :: Int
                   , w_pid     :: Int
                   , w_nmove   :: Int
                   , w_dir     :: FilePath
                   , w_moves   :: [Move]
                   , w_punter  :: PunterId -- our punter id
                   }

render :: SiteMap -> IO ()
render sm = do
  playIO
    (InWindow "Site Map" (round $ canvas_size, round $ canvas_size) (10, 10))
    white
    10
    World { w_sitemap = sm
          , w_punters = 0
          , w_pid     = 0
          , w_nmove   = 0
          , w_dir     = ""
          , w_moves   = []
          , w_punter  = PunterId 0
          }
    renderWorld
    kbdControl
    (\x w -> return w)

renderLastSession :: IO ()
renderLastSession = do
  let logdir = "data/lastsession"
  sm  <- read <$> readFile (logdir </> "sitemap")
  ps  <- read <$> readFile (logdir </> "punters")
  puid  <- read <$> readFile (logdir </> "punter")
  playIO
    (InWindow "Site Map" (round $ canvas_size, round $ canvas_size) (10, 10))
    white
    10
    World { w_sitemap = sm
          , w_punters = ps
          , w_pid     = 0
          , w_nmove   = 0
          , w_dir     = logdir
          , w_moves   = []
          , w_punter  = puid
          }
    renderWorld
    kbdControl
    (\x w -> return w)
