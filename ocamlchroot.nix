
{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let

  fhs = pkgs.buildFHSUserEnv {
    name = "fhs";
    targetPkgs = pkgs:
      with pkgs;
      with pkgs.ocamlPackages;
      [
        git findlib opam ocaml ocamlbuild yojson ocaml_lwt
        gnumake curl patch unzip m4 which gcc libevent jbuilder libev ncurses camlp4
        pkgconfig rsync
      ];
    runScript = "bash";
    profile = ''
      export USE_CCACHE=1
      export LIBRARY_PATH=/usr/lib64
      export LD_LIBRARY_PATH=`pwd`:/usr/lib64
      export STACK_ROOT=`pwd`/.stack-root
      export ACLOCAL_PATH=/usr/share/aclocal
      export LANG=C
      export LC_ALL=en_US.utf-8
    '';
  };

in
stdenv.mkDerivation {
  name = fhs.name + "-env";
  nativeBuildInputs = [ fhs ];
  shellHook = "exec fhs";
}

