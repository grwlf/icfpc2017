#!/bin/sh

NPUNTER=2

runpunt() { (
  local port=$1
  local ndummies=16
  mkdir logs 2>/dev/null || true
  touch logs/punter-$port.log
  ln -f -s logs/punter-$port.log punter.log
  tail -f logs/punter-$port.log &
  pid=$!
  trap "kill $pid" EXIT
  ./dist/build/punter/punter $NPUNTER $port $ndummies >logs/punter-$port.log 2>&1
) }

sexp() { expr "$@" >/dev/null ; }

joinmap() {
  local map=$1
  shift
  local i=0
  for port in `shuf -e $2` ; do
    if sexp "$i" '>=' "$1" ; then
      echo $i
      return 0
    fi
    echo -n "Trying map $map attempt $i port $port .. "
    if runpunt $port 16 ; then
      echo "passed"
    else
      echo "unsuccessfull"
      tail -n 5 "logs/punter-$port.log"
      continue
    fi
    i=`expr $i + 1`
  done
  return 1
}

set -e

NGAMES=1
joinmap 'map.json' $NGAMES "`seq -s ' ' 9847 1 9863`"

# joinmap 'gothenburg-sparse.json' $NGAMES "`seq -s ' ' 9847 1 9863`"
joinmap 'randomMedium.json' $NGAMES "`seq -s ' ' 9041 1 9050`"

# idx=`shuf -e $(seq -s ' ' 0 1 100) | head -n 1`
# case `expr 12 % $idx` in
#   0) joinmap 'edinburgh-sparse.json' $NGAMES "`seq -s ' ' 9081 1 9090`";;
#   1) joinmap 'edinburgh-sparse.json' $NGAMES "`seq -s ' ' 9091 1 9100`";;
#   2) joinmap 'sample.json' $NGAMES "`seq -s ' ' 9001 1 9010`" ;;
#   3) joinmap 'lambda.json' $NGAMES "`seq -s ' ' 9011 1 9020`" ;;
#   4) joinmap 'Sterp' $NGAMES "`seq -s ' ' 9021 1 9030`" ;;
#   5) joinmap 'circle.json' $NGAMES "`seq -s ' ' 9031 1 9040`" ;;
#   6) joinmap 'randomMedium.json' $NGAMES "`seq -s ' ' 9041 1 9050`" ;;
#   7) joinmap 'randomSParse.json' $NGAMES "`seq -s ' ' 9051 1 9060`" ;;
#   8) joinmap 'boston-sparse.json' $NGAMES "`seq -s ' ' 9061 1 9070`" ;;
#   9) joinmap 'tube.json' $NGAMES "`seq -s ' ' 9071 1 9080`" ;;
#   10) joinmap 'oxford-sparse.json' $NGAMES "`seq -s ' ' 9221 1 9230`" ;;
#   11) joinmap 'gothenburg-sparse.json' $NGAMES "`seq -s ' ' 9595 1 9647`" ;;
#   12) joinmap 'nara-sparse.json' $NGAMES "`seq -s ' ' 9211 1 9220`" ;;
# esac

# runpunt `shuf -e 9001 9005 9009 9010` 16
# runpunt 9196


echo OK




