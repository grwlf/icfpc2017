{ nixpkgs ? import <nixpkgs> {},
  ghc ? "ghc801",
  force_build ? false }:

let

  inherit (nixpkgs) pkgs;

  vkhs = { mkDerivation, aeson, aeson-pretty, base, bytestring
      , case-insensitive, clock, containers, data-default-class
      , directory_1_3_1_1, filepath, http-client, http-client-tls, http-types
      , mtl, network-uri, optparse-applicative, parsec, pipes, pipes-http
      , pretty-show, regexpr, split, stdenv, taglib, tagsoup, text, time
      , utf8-string, vector, cabal-install, zlib, haskdogs, hasktags, scientific
      , hdevtools, MonadRandom, lens, mersenne-random-pure64, monad-loops
      , tasty, tasty-hunit, tasty-quickcheck , gloss , network-simple , binary
      , base64-bytestring , psqueues , astar, QuickCheck, random-shuffle
      , monadplus, pqueue, deepseq
      }:
      mkDerivation {
        pname = "icfpc2017";
        version = "0.1";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        extraLibraries = [taglib zlib];
        libraryHaskellDepends = [
          aeson aeson-pretty base bytestring case-insensitive clock
          containers data-default-class directory_1_3_1_1 filepath http-client
          http-client-tls http-types mtl network-uri optparse-applicative
          parsec pipes pipes-http pretty-show split taglib tagsoup time
          utf8-string vector cabal-install zlib scientific hdevtools
          MonadRandom lens mersenne-random-pure64 monad-loops tasty tasty-hunit
          gloss network-simple binary base64-bytestring psqueues astar tasty-quickcheck
          QuickCheck random-shuffle monadplus pqueue deepseq
        ];
        executableHaskellDepends = [ regexpr text haskdogs hasktags ];
        executableToolDepends = [ haskdogs hasktags ];
        doHaddock = false;
        description = "Provides access to Vkontakte social network via public API";
        license = stdenv.lib.licenses.bsd3;

        shellHook=''
          if test -f /etc/myprofile ; then
            . /etc/myprofile
          fi

          export LIBRARY_PATH=${pkgs.zlib}/lib:${pkgs.taglib}/lib
        '';
      };

  drv = p : pkgs.haskell.packages.${ghc}.callPackage p {};

in

  if !force_build && pkgs.lib.inNixShell then (drv vkhs).env else (drv vkhs)

