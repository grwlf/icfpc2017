{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

{-# OPTIONS_GHC -Wno-missing-methods #-}

module Main (main) where

import qualified Data.ByteString as BS

import Test.Tasty (TestTree, defaultMain, testGroup)
import Test.Tasty.HUnit (assertEqual, testCase, (@?=))
import Test.Tasty.QuickCheck
import Test.QuickCheck (Arbitrary, Gen, Property, arbitrary, classify,
                        elements, forAll, forAllShrink, frequency,
                        getNonNegative, oneof, quickCheckAll, resize,
                        sized)

import Data.Time

import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Types as Aeson
import qualified Data.Vector as V
import qualified Data.HashMap.Strict as M
import qualified Data.Set as S

import qualified SiteGraph2 as SiteGraph2

import Algo.AStar
import Algo.Ranking
import qualified Algo.ClusterGraph as CG
import Algo.ClusterGraph(ClusterGraph,ClusterId(..))

import Imports
import Types
import SiteGraph
import FloodFill
import Client


main :: IO ()
main = testSuiteMain

testSuiteMain :: IO ()
testSuiteMain = do
    defaultMain $
        testGroup "tests" [small_tests, big_tests, quickcheck_tests, cluster_graph] -- ++ [benchmarks]

small_tests = testGroup "small_tests" [
            testCase "map-load" $ withTestMap $ \sm -> do
              map_site (SiteId 1) sm @?= Site 1 1.0 0.0

          , testCase "map-proc" $ withTestMap $ \sm -> do
              let sg = buildSiteGraph sm
              getNeighs (SiteId 0) sg @?= map SiteId [1, 7]
              getNeighs (SiteId 1) sg @?= map SiteId [0, 2, 3, 7]
              getNeighs (SiteId 5) sg @?= map SiteId [3, 4, 6, 7]

          , testCase "flood-fill" $ withTestMap $ \sm -> do
              let sg = buildSiteGraph sm
              let dists = floodFill sg (SiteId 1)
              dists @?= V.fromList [1,0,1,1,2,2,2,1]

          , testCase "map-rank" . withTestMap $ \sm -> do
              let rmap = rankBySite (SiteId 1) $ buildSiteGraph sm
              getRank (SiteId 0) rmap @?= 1
              getRank (SiteId 1) rmap @?= 0
              getRank (SiteId 2) rmap @?= 1
              getRank (SiteId 3) rmap @?= 1
              getRank (SiteId 4) rmap @?= 2
              getRank (SiteId 5) rmap @?= 2
              getRank (SiteId 6) rmap @?= 2
              getRank (SiteId 7) rmap @?= 1

          ,  testCase "remove-river" $ withTestMap $ \sm -> do
              let sg = buildSiteGraph sm
              let sg' = removeRiver (River (SiteId 0) (SiteId 1)) sg
              let dists = floodFill sg' (SiteId 1)
              dists @?= V.fromList [2,0,1,1,2,2,2,1]

          ,  testCase "map-serialize" $ withTestMap $ \sm -> do
              let sg = buildSiteGraph sm
              let sg2 = bin2st $ st2bin sg :: SiteGraph
              (M.keys sg2) @?= (M.keys sg)
              (map ss_neighbors $ M.elems sg2) @?= (map ss_neighbors $ M.elems sg)
          ]

big_tests = testGroup "big_tests" $ [
            testCase "rank" $ withBigMap $ \sm -> do
              let rmap = rankBySite (SiteId 1) $ buildSiteGraph sm
              print $ getRank (SiteId 0) rmap

          , testCase "floodfill" $ withBigMap $ \sm -> do
              let rmap = floodFill (buildSiteGraph sm) (SiteId 1)
              print $ rmap V.! 0
          ]

benchmarks = testGroup "benchmarks" $ [
            testCase "floodfill vs rank" $ withBigMap $ \sm -> do
              let sg = buildSiteGraph sm
              let rmap = floodFill sg (SiteId 1)
              let rmap2 = rankBySite (SiteId 1) sg
              print $ getRank (SiteId 0) rmap2
              print $ rmap V.! 0

              t1 <- getCurrentTime
              let rmap = rankBySite (SiteId 1) sg
              print $ getRank (SiteId 2000) rmap

              t2 <- getCurrentTime
              let rmap = floodFill sg (SiteId 1)
              print $ rmap V.! 2000

              t3 <- getCurrentTime
              print $ "rank: " ++ show (diffUTCTime t2 t1)
              print $ "floodfill: " ++ show (diffUTCTime t3 t2)
          ]

quickcheck_tests = testGroup "quickcheck" $ [
            testProperty "mapConversion" (
              \(sm :: SiteMap) ->
                let
                  sg = SiteGraph2.buildSiteGraph sm
                in
                (bin2st $ st2bin sg) == sg
            )
          ]

withTestMap act = do
  r <- pure $ Aeson.eitherDecode map1
  case r of
    Right (mp :: SiteMap)-> act mp
    Left err -> do
      fail err

withBigMap act = do
  let file = "./maps/oxford2-sparse-2.json"
  map_data <- BS.readFile file
  case Aeson.eitherDecodeStrict map_data of
    Right (sm :: SiteMap) -> act sm
    Left e -> fail e

map1 = [heredoc|
  {   "sites": [     {"id": 0, "x": 0.0, "y": 0.0},     {"id": 1, "x": 1.0, "y": 0.0},     {"id": 2, "x": 2.0, "y": 0.0},     {"id": 3, "x": 2.0, "y": -1.0},     {"id": 4, "x": 2.0, "y": -2.0},     {"id": 5, "x": 1.0, "y": -2.0},     {"id": 6, "x": 0.0, "y": -2.0},     {"id": 7, "x": 0.0, "y": -1.0}   ],   "rivers": [     { "source": 0, "target": 1},     { "source": 1, "target": 2},     { "source": 0, "target": 7},     { "source": 7, "target": 6},     { "source": 6, "target": 5},     { "source": 5, "target": 4},     { "source": 4, "target": 3},     { "source": 3, "target": 2},     { "source": 1, "target": 7},     { "source": 1, "target": 3},     { "source": 7, "target": 5},     { "source": 5, "target": 3}   ],   "mines": [1, 5] }
|]

instance Arbitrary SiteId where
  arbitrary = arbitrary `suchThat` (\(SiteId x)->(x>=0))

instance Arbitrary Site where
  arbitrary = Site
    <$> (arbitrary`suchThat`(>=0))
    <*> arbitrary
    <*> arbitrary

instance Arbitrary SiteMap where
  arbitrary = anySiteMap

anySiteMap :: Gen SiteMap
anySiteMap = do
  n <- choose (1,10)
  sites <- nub <$> vectorOf n arbitrary
  let sitegen = siteId <$> elements sites
  rivers <- filter (\(River a b) -> a/=b) <$> listOf (River <$> sitegen <*> sitegen)
  mines <- map siteId <$> sublistOf sites
  return $
    SiteMap
      (M.fromList $ map (\x -> (siteId x, x)) sites)
      (V.fromList rivers)
      (V.fromList mines)


--CLUSTER_GRAPH

instance Num SiteId where
  fromInteger x = SiteId (fromInteger x)

instance Num ClusterId where
  fromInteger x = ClusterId (fromInteger x)

test_sg = M.fromList $
          map (\(s,ns) -> (SiteId s, SiteStruct {ss_siteId = SiteId s
                                                , ss_siteX = 0
                                                , ss_siteY = 0
                                                , ss_neighbors = S.fromList $ map SiteId ns
                                                , ss_isMine = False
                                                } )) map_data

 where map_data = [(1,[2,4])
                  ,(2,[1,3])
                  ,(3,[2,4])
                  ,(4,[3,1])]

cluster_graph = testGroup "ClusterGraph" $ [
  testCase "build" $ do
      sort (CG.getNeighs 1 cg) @?= [2, 4]
      CG.getRivers 1 2 cg @?= [River 1 2]
      CG.getRivers 1 3 cg @?= []
  ,
  testCase "remove_river" $ do
      let cg' = CG.removeRiver (River 1 2) cg
      CG.getNeighs 1 cg' @?= [4]
  ,
  testCase "merge_river" $ do
      let cg' = CG.mergeClusters 1 4 cg
      CG._cg_scmap cg' @?= M.fromList [(SiteId 1, ClusterId 1), (4, 1), (2, 2), (3,3)]
      CG.cgSites 1 cg @?= S.fromList [1]
      CG.getNeighs 1 cg' @?= [2,3]
      CG.cgSites 1 cg' @?= S.fromList [1,4]
      CG.getNeighs 2 cg' @?= [1,3]
  ,
  testCase "merge_rivers" $ do
      let cg' = CG.mergeClusters 1 4 cg
          cg'' = CG.mergeClusters 2 3 cg'

      CG.getNeighs 1 cg'' @?= [2]
      CG.getNeighs 2 cg'' @?= [1]
      CG.cgSites 1 cg'' @?= S.fromList [1,4]
      CG.cgSites 2 cg'' @?= S.fromList [2,3]
      CG.getRivers 1 2 cg'' @?= [River 1 2, River 3 4]
  ,
  testCase "remove_river" $ do
      let cg' = CG.removeRiver (River 1 2) cg
          cg'' = CG.removeRiver (River 3 4) cg'
          p = cgAStar cg 1 [2]
          p' = cgAStar cg' 1 [2]
          p'' = cgAStar cg'' 1 [2]
          p''' = cgAStar cg 1 [2,3]

      p @?= Just [1,2]
      p' @?= Just [1,4,3,2]
      p'' @?= Nothing
      p''' @?= Just [1,2]
  ]
  where cg = CG.buildClusterGraphInner test_sg []
