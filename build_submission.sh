#!/usr/bin/env bash
set -e

BINARY=punter

# registration form:
#http://punter.inf.ed.ac.uk:9000/update/?token=fc67862f-c0ff-4bb8-aad4-0a0e2311bfeb

TEAM_ID=fc67862f-c0ff-4bb8-aad4-0a0e2311bfeb

VM=punter@192.168.1.72
SUB_DIR=dist/submission
ICFP_DIR=dist/submission/icfp
BUILD_DIR=repo-`whoami`

SUB_FILE=dist/icfp-${TEAM_ID}.tar.gz

rm -f $SUB_FILE
rm -rf $SUB_DIR
mkdir -p $SUB_DIR

git archive HEAD > ${SUB_DIR}/repo.tar
scp ${SUB_DIR}/repo.tar ${VM}:
ssh $VM rm -rf $BUILD_DIR
ssh $VM mkdir $BUILD_DIR
ssh $VM tar xf repo.tar -C $BUILD_DIR
ssh $VM sed -i "s/buildable:True/buildable:False/" $BUILD_DIR/icfpc2017.cabal
ssh $VM "cd $BUILD_DIR && cabal build"

mkdir -p $SUB_DIR/icfp
echo "#$/usr/bin/env bash" > $ICFP_DIR/install
chmod +x $ICFP_DIR/install
scp ${VM}:${BUILD_DIR}/dist/build/${BINARY}/${BINARY} $ICFP_DIR/punter
mkdir -p $ICFP_DIR/src
cp -r src app $ICFP_DIR/src
cat >$ICFP_DIR/README <<EOF
VIM Team members:

Dmitry Vyal
Sergey Mironov
Vladimir Ivanov

---
`git log HEAD^..HEAD`
---
EOF
tar czf ${SUB_FILE} -C $ICFP_DIR .
scp ${SUB_FILE} ${VM}:

