{-# LANGUAGE BangPatterns #-}
module FloodFill where

import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import qualified Data.Set as S

import Control.Monad.ST

import SiteGraph
import Types

floodFill :: SiteGraph -> SiteId -> V.Vector Int
floodFill graph start =
  let size = s_id (getMaxSiteId graph) + 1
      vec = V.replicate size (-1)
  in V.modify (\v -> floodFill' v S.empty (S.singleton start) 0) vec

 where
   floodFill' v visited front _ | null front = return ()
   floodFill' v visited front (!cnt) = do
     let neighs = S.unions $ map (getNeighSet `flip` graph) $ S.toList front :: S.Set SiteId
         visited2 = S.union visited front  :: S.Set SiteId
         front2 = S.difference neighs visited2 :: S.Set SiteId

     mapM_ (\k -> VM.write v (s_id k) cnt) $ S.toList front
     floodFill' v visited2 front2 (cnt+1)
