{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module SiteGraph2 where

import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Set as Set
import qualified Data.Vector as Vector
import qualified Data.Binary as Binary

import Types
import Imports

data SiteStruct = SiteStruct {
    ss_siteId    :: !SiteId
  , ss_neighbors :: Set SiteId
  , ss_isMine    :: Bool
  }
  deriving (Show,Generic,Binary,Eq,Ord)

invalidSiteStruct = SiteStruct invalidSiteId mempty False


type SiteGraph = HashMap SiteId SiteStruct

-- ACCESSORS

getNeighs :: SiteId -> SiteGraph -> [SiteId]
getNeighs s g = Set.toList $ getNeighSet s g

getNeighSet :: SiteId -> SiteGraph -> Set SiteId
getNeighSet s g = ss_neighbors $ fromMaybe invalidSiteStruct $ HashMap.lookup s g

getMaxSiteId :: SiteGraph -> SiteId
getMaxSiteId = maximum . HashMap.keys

-- MODIFIERS

removeRiver :: River -> SiteGraph -> SiteGraph
removeRiver (River s t) sg = sg''
  where
    sg' = HashMap.adjust (remove_neigh t) s sg
    sg'' = HashMap.adjust (remove_neigh s) t sg'
    remove_neigh n ss = ss { ss_neighbors = Set.delete n (ss_neighbors ss) }

-- CONSTRUCTOR
--
buildSiteGraph :: SiteMap -> SiteGraph
buildSiteGraph smap = setNeighbors . setMines . setSites $ HashMap.empty
  where setSites :: SiteGraph -> SiteGraph
        setSites xs = foldr ((uncurry HashMap.insert .
                              ((SiteId . site_id) &&& toSiteStruct)) . snd) xs .
                      HashMap.toList . map_sites $ smap
                      where
                        toSiteStruct s = SiteStruct {
                            ss_siteId    = SiteId $ site_id s
                          , ss_neighbors = Set.empty
                          , ss_isMine    = False
                          }

        setMines :: SiteGraph -> SiteGraph
        setMines xs = foldr (HashMap.adjust $ \s -> s { ss_isMine = True }) xs .
                      Vector.toList . map_mines $ smap

        setNeighbors :: SiteGraph -> SiteGraph
        setNeighbors xs = foldr setEdge xs . Vector.toList . map_rivers $ smap
          where setEdge river =
                  HashMap.adjust (setN to) from . HashMap.adjust (setN from) to
                  where from = river_target river
                        to   = river_source river
                        setN n s = s { ss_neighbors = Set.insert n $ ss_neighbors s }



