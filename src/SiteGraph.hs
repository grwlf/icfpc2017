{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module SiteGraph where

import           Control.Arrow       ((&&&))
import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import           Data.Set            (Set)
import qualified Data.Set            as S
import qualified Data.Vector         as V

import           Types
import           Imports
import qualified Data.Binary as Binary

data SiteStruct = SiteStruct
                  { ss_siteId    :: {-# UNPACK #-} !SiteId
                  , ss_siteX     :: {-# UNPACK #-} !Float
                  , ss_siteY     :: {-# UNPACK #-} !Float
                  , ss_neighbors :: Set SiteId
                  , ss_isMine    :: Bool
                  }
                deriving (Show,Generic,Binary)

type SiteGraph = HashMap SiteId SiteStruct

-- instance Binary SiteGraph where
--   put x = Binary.put $ M.toList x
--   get = M.fromList <$> Binary.get

-- ACCESSORS

getNeighs :: SiteId -> SiteGraph -> [SiteId]
getNeighs s g = S.toList $ getNeighSet s g

getNeighSet :: SiteId -> SiteGraph -> Set SiteId
getNeighSet s g = ss_neighbors $ g M.! s


getMaxSiteId :: SiteGraph -> SiteId
getMaxSiteId = maximum . M.keys

-- MODIFIERS

removeRiver :: River -> SiteGraph -> SiteGraph
removeRiver (River s t) sg = sg''
  where
    sg' = M.adjust (remove_neigh t) s sg
    sg'' = M.adjust (remove_neigh s) t sg'
    remove_neigh n ss = ss { ss_neighbors = S.delete n (ss_neighbors ss) }

-- CONSTRUCTOR
buildSiteGraph :: SiteMap -> SiteGraph
buildSiteGraph smap = setNeighbors . setMines . setSites $ M.empty
  where setSites :: SiteGraph -> SiteGraph
        setSites xs = foldr ((uncurry M.insert .
                              ((SiteId . site_id) &&& toStruct)) . snd) xs .
                      M.toList . map_sites $ smap
          where toStruct s = SiteStruct { ss_siteId    = SiteId $ site_id s
                                        , ss_siteX     = site_x s
                                        , ss_siteY     = site_y s
                                        , ss_neighbors = S.empty
                                        , ss_isMine    = False
                                        }

        setMines :: SiteGraph -> SiteGraph
        setMines xs = foldr (M.adjust $ \s -> s { ss_isMine = True }) xs .
                      V.toList . map_mines $ smap

        setNeighbors :: SiteGraph -> SiteGraph
        setNeighbors xs = foldr setEdge xs . V.toList . map_rivers $ smap
          where setEdge river =
                  M.adjust (setN to) from . M.adjust (setN from) to
                  where from = river_target river
                        to   = river_source river
                        setN n s = s { ss_neighbors = S.insert n $ ss_neighbors s }

