{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NondecreasingIndentation #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
module Client where

import Network.Simple.TCP
import System.IO
import System.Environment
import Control.Monad
import Data.ByteString.Lazy (fromStrict,toChunks)
import System.FilePath ((</>))
import System.Directory
import System.Environment
import qualified Data.ByteString.Char8 as ByteString
import qualified Data.Aeson as Aeson
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import qualified Data.ByteString.Base64 as B64
import qualified Data.Binary as Binary

import Imports
import Types

debug :: (MonadReader ClientOptions m, MonadIO m) => String -> m ()
debug s = do
  co <- ask
  when (not $ co_silent co) $ do
    liftIO $ hPutStrLn stderr s

class Channel x where
  -- | send a bytestring
  channelSend :: (MonadIO m) => x -> ByteString -> m ()
  -- | recv AT_MOST x bytes of data
  channelRecv :: (MonadIO m) => x -> Int -> m (Maybe ByteString)

instance Channel Socket where
  channelSend = send
  channelRecv = recv

-- | stdin,stdout case
instance Channel (Handle,Handle) where
  channelSend (i,o) bs = liftIO $ ByteString.hPutStr o bs >> hFlush o
  channelRecv (i,o) maxlen = liftIO $ Just <$> ByteString.hGet i maxlen

sendMsg :: (Channel s, MonadIO m) => s -> ByteString -> m ()
sendMsg sock msg = do
  channelSend sock ((bshow $ ByteString.length msg) <> ":" <> msg)

recvMsg :: (Channel s, MonadIO m) => s -> m ByteString
recvMsg sock = do

  snd <$> do
  flip execStateT (Nothing, ByteString.empty) $ do
  whileM $ do
    (mlen, bs) <- get
    case mlen of
      Nothing -> do
        res <- channelRecv sock 10
        case res of
          Nothing -> fail "Connection closed on error while receiving header"
          Just header -> do
            (h,t) <- pure $ ByteString.span (/=':') (bs<>header)
            case ByteString.length t > 0 of
              True -> do -- got length ':' tail
                -- traceM $ "recv header:" <> (bunpack $ bs<>header)
                -- traceM $ "recv len:" <> (bunpack h)
                put (Just (bread h), ByteString.drop 1 t)
              False -> do
                put (Nothing, bs<>header)
            return True

      Just len -> do
        res <- channelRecv sock (len - ByteString.length bs)
        case res of
          Just bs' -> do
            -- debug $ "Chunk received"
            bs2 <- pure $ bs <> bs'
            -- traceM $ "recv body:" <> (bunpack $ bs2)
            put (Just len, bs2)
            return ((ByteString.length bs2) < len)
          Nothing -> do
            fail "Connection closed or error"

recvJS :: (Channel s, FromJSON a, MonadIO m) => s -> m a
recvJS sock = do
  bs <- liftIO $ recvMsg sock
  ret <- pure $ Aeson.eitherDecodeStrict bs
  case ret of
    Right a -> return a
    Left err -> fail $ err <> "\nwhile parsing\n" <> bunpack bs

sendJS :: (Channel s, ToJSON a, MonadIO m) => s -> a -> m ()
sendJS sock json = do
  bs <- pure $ Aeson.encode json
  sendMsg sock (ByteString.concat $ toChunks $ bs)

data ClientOptions = ClientOptions {
    co_silent :: Bool
  , co_sigmvar :: Maybe (MVar ())
  }

defaultClientOptions mv = ClientOptions {
    co_silent = False
  , co_sigmvar = mv
  }

offlineClientOptions mv = (defaultClientOptions Nothing)

silentClientOptions = (defaultClientOptions Nothing) {
    co_silent = True
  }


st2bin :: (Binary s) => s -> Text
st2bin sg = Text.decodeUtf8 $ B64.encode $ ByteString.concat $ toChunks $ Binary.encode sg

bin2st :: (Binary s) => Text -> s
bin2st t =  Binary.decode $ fromStrict $ B64.decodeLenient $ Text.encodeUtf8 t


-- Shouldn be used from offline
logInit :: (MonadReader ClientOptions m, MonadIO m)
        => FilePath -> SiteMap -> PunterId -> Int -> m ()
logInit fp sm pid ps = do
  ClientOptions{..} <- ask
  when (not co_silent) $ do
    liftIO $ do createDirectoryIfMissing True fp
                listDirectory fp >>= mapM_ (removeFile . (fp </>))
                writeFile (fp </> "sitemap") (show sm)
                writeFile (fp </> "punters") (show ps)
                writeFile (fp </> "punter") (show pid)

-- Shouldn be used from offline
logWrite :: (MonadReader ClientOptions m, MonadIO m) => FilePath -> Int -> [Move] -> m ()
logWrite logdir nmove moves = do
  ClientOptions{..} <- ask
  when (not co_silent) $ do
    forM_ moves $ \mv -> do
      let file = logdir </> (printf "move_%02d_pid%01d" nmove $ p_id $ getPid mv)
      liftIO $ writeFile file (show mv)
        where getPid :: Move -> PunterId
              getPid (MoveClaim claim) = claim_punter claim
              getPid (MovePass pass)   = pass_punter pass
              getPid (MoveOption claim) = claim_punter claim
              getPid _ = PunterId (-1)

data OnlineState = OnlineState {
    _os_state :: Text
  , _os_nmove :: Int
  , _os_maxstate_sz :: Int
  , _os_scores :: Maybe Int
  }

$(makeLenses ''OnlineState)

online :: (Binary s)
  => String
  -> ClientOptions
  -> (PunterId -> SiteMap -> Extensions -> IO s)
  -> (UTCTime -> (Gameplay_Req_S s) -> IO (Gameplay_Res_S s))
  -> IO (Maybe Int)
online port co s0gen punt = do
  connect "punter.inf.ed.ac.uk" port $ \(s, remoteAddr) -> do
    flip runReaderT co $ do
      debug $ "Connection established to " <> show remoteAddr
      sendMsg s "{\"me\":\"vim\"}"
      (d :: Handshake) <- recvJS s
      debug $ "Handshake received"
      req <- recvJS s
      let pid = (setup_req_punterid req)
      debug $ "Setup request received, pid " <> show pid
      let extensions = Extensions (hasOptions req)
      debug $ "Setup extensions " <> show extensions
      sendJS s (Setup_Res pid)
      debug $ "Setup response sent"
      let logfp = "data/lastsession"
      logInit logfp (setup_req_map req) (setup_req_punterid req) (setup_req_punters req)
      case co_sigmvar co of
        Nothing ->
          return ()
        Just mv -> do
          liftIO $ putMVar mv ()
      s0 <- liftIO $ s0gen pid (setup_req_map req) extensions
      let os0 = OnlineState (st2bin s0) 0 0 Nothing
      view os_scores <$> do
      flip execStateT os0 $ do
        whileM $ do
          nmove <- use os_nmove
          (req :: Req) <- recvJS s
          case req of
            ReqGameplay gp -> do
              debug $ "Moves received"
              tstart <- liftIO $ getCurrentTime
              st <- bin2st <$> use os_state
              res <- liftIO $ punt tstart (Gameplay_Req_S (gp_req_moves gp) st)
              sendJS s (Gameplay_Res (gp_res_move_s res))
              debug $ "Moves send: " <> printMove (gp_res_move_s res)
              let st2 = st2bin $ gp_res_state res
              os_state %= const st2
              logWrite logfp nmove (gp_req_moves gp)
              os_nmove %= (+1)
              os_maxstate_sz %= (max (Text.length st2))
              return True
            ReqScoring Scoring_Req{..} -> do
              maxsz <- use os_maxstate_sz
              let (our_score, other_scores) = scoresGain pid scoring_scores
              debug $ "Scoring received\n" <> ppShow scoring_scores <> "\n"
              debug $ "Max serialized size " <> show maxsz <> "\n"
              debug $ "Opponent score " <> show other_scores <> "\n"
              debug $ "PunterId " <> show (p_id pid) <> " score " <> show our_score <> "\n"
              os_scores %= const our_score
              return False
            ReqTimeout Timeout{..} -> do
              debug $ "Timeout " <> show timout_len <> " sec(?)"
              return True

offline :: (Show s, Binary s)
  => (PunterId -> SiteMap -> Extensions -> IO s) -- initial state generator
  -> (UTCTime -> (Gameplay_Req_S s) -> IO (Gameplay_Res_S s))
  -> IO (Maybe Int)
offline s0gen punt = do
  let co = offlineClientOptions Nothing
  let s = (stdin,stdout)
  flip runReaderT co $ do
    sendMsg s "{\"me\":\"vim\"}"
    debug $ "Handshake sent"
    (hs :: Handshake) <- recvJS s
    debug $ "Handshake received"
    req <- recvJS s
    tstart <- liftIO $ getCurrentTime
    debug $ "Geamplay request received"
    case req of
      ReqSetup_S req@Setup_Req{..} -> do
        let pid = setup_req_punterid
        debug $ "Setup request received, pid " <> show pid
        let extensions = Extensions (hasOptions req)
        debug $ "Setup extensions " <> show extensions
        s0 <- lift $ s0gen pid setup_req_map extensions
        sendJS s . Setup_Res_S pid . Aeson.toJSON . st2bin $ s0
        debug $ "Setup response sent"
        return (Just 0)
      ReqGameplay_S gp -> do
        debug $ "Moves received "
        gp2 <- liftIO $ punt tstart (Gameplay_Req_S (gp_req_moves_s gp) (bin2st $ gp_req_state gp))
        sendJS s (Gameplay_Res_S (gp_res_move_s gp2) (st2bin $ gp_res_state gp2))
        debug $ "Moves sent: " <> printMove (gp_res_move_s gp2)
        return (Just 0)
      ReqScoring_S sc -> do
        debug $ "Scoring received " <> show sc
        return (Just 0)
      x -> do
        debug $ "Unknown received " <> show x
        return Nothing

passbot :: UTCTime -> (Gameplay_Req_S PunterId) -> IO (Gameplay_Res_S PunterId)
passbot _ Gameplay_Req_S{..} = do
  return (Gameplay_Res_S (movePass gp_req_state) gp_req_state)


