{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Algo.Heuristics where

import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import Data.Hashable(Hashable)
import GHC.Generics(Generic)
import Data.Maybe(catMaybes)
import Data.List(sortBy)
import           System.Random.Shuffle

import Algo.ClusterGraph
import Algo.AStar

-- | Returns unclaimed rivers across which most paths between mines go
hottestRivers :: ClusterGraph -> IO [(HyperRiver, Int)]
hottestRivers cg = do
  let mines = getMines cg
  rmines <- shuffleM mines
  let
    rmines' = take 4 rmines
    path2hrs path = zipWith mkHyperRiver path (tail path)

    mine_pairs = [ (m1,m2) | m1<-rmines', m2<-rmines', m1 < m2 ]


    path_hrivers = concatMap path2hrs $ catMaybes $ map (\(m1,m2) -> cgAStar cg m1 [m2]) mine_pairs
    rivers_rating = M.toList $ M.fromListWith (+) $ map (\r -> (r,1)) path_hrivers

  return $ sortBy (\rr1 rr2 -> compare (snd rr2) (snd rr1)) rivers_rating
