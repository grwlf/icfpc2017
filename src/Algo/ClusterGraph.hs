{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}
module Algo.ClusterGraph where

import Data.Monoid ((<>))
import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import           Data.Set            (Set)
import qualified Data.Set            as S
import qualified Data.Vector         as V
import qualified Data.Binary as Binary
import qualified Data.PQueue.Prio.Min as MinPQueue
import qualified Data.PQueue.Prio.Max as MaxPQueue
import Data.Maybe(fromMaybe)
import Data.PQueue.Prio.Max(MaxPQueue)
import Data.Binary(Binary)
import GHC.Generics (Generic)
import Data.Hashable(Hashable)
import Control.Lens (Lens', makeLenses, (%=), (^.), over, view, use, uses, zoom, _1, _2, _3, _4, _5, _6)
import qualified Control.Lens as Lens
import Control.Monad(mapM)
import Control.DeepSeq (NFData)
import qualified Data.List as L

import Types
import SiteGraph(SiteGraph, SiteStruct(..), buildSiteGraph)
import Algo.RankMap(RankMap(..), rankBySite)

newtype ClusterId = ClusterId {c_id :: Int}
  deriving (Eq, Ord, Show, Generic, Binary, Hashable, NFData)

site2cluster = ClusterId . s_id

invalidClusterId = ClusterId (-1)

-- Ordered river
data ORiver = ORiver {
    or_s :: !SiteId, or_t :: !SiteId
  }
  deriving (Show, Generic, Binary, Eq, Ord, NFData)

-- River consiting of hyper edge
data HyperRiver = HyperRiver { hr_c1::ClusterId, hr_c2 :: ClusterId } deriving (Show,Eq,Hashable,Generic)
mkHyperRiver c1 c2 = if c_id c1 <= c_id c2 then HyperRiver c1 c2 else HyperRiver c2 c1

mkORiver :: River -> ORiver
mkORiver (River s1 s2) | s1 < s2 = ORiver s1 s2
                       | True = ORiver s2 s1

or2r (ORiver s1 s2) = River s1 s2

type ClusterMap = HashMap ClusterId SiteCluster
type SCMap = HashMap SiteId ClusterId
type CNeighMap = HashMap ClusterId (Set ORiver)

type CScoreMaps = HashMap ClusterId CScoreMap
type CScoreMap = HashMap ClusterId Int


data SiteCluster = SiteCluster
  { _sc_sites :: Set SiteId
  , _sc_mines :: Set SiteId
  , _sc_neighbors :: CNeighMap
  } deriving (Show, Generic, Binary,NFData)

invalidCluster = SiteCluster mempty mempty mempty

$(makeLenses ''SiteCluster)

data ClusterGraph = ClusterGraph
  { _cg_clusters :: ClusterMap
  , _cg_scoremaps :: CScoreMaps
  , _cg_scmap :: SCMap
  } deriving (Show,Generic,Binary,NFData)

$(makeLenses ''ClusterGraph)

-- ACCESSORS

assert :: String -> a -> a
-- FIXME disable errors in release
-- assert msg def = def
assert msg def = error $ "assert: " <> msg

safeLookup msg def k v = fromMaybe (assert msg def) $ M.lookup k v

oriver2hriver :: ORiver -> ClusterGraph -> HyperRiver
oriver2hriver (ORiver s t) ClusterGraph{..} = mkHyperRiver c1 c2
  where c1 = _cg_scmap M.! s
        c2 = _cg_scmap M.! t

-- | Returns neighbouring clusters
getNeighs :: ClusterId -> ClusterGraph -> [ClusterId]
getNeighs s sg = fromMaybe (assert "getNeighs" []) $ M.keys <$>_sc_neighbors <$> M.lookup s (_cg_clusters sg)

-- | Returns clusters with mines
getMines :: ClusterGraph -> [ClusterId]
getMines ClusterGraph{..} = M.keys _cg_scoremaps

-- | Returns real rivers joining two clusters
getRivers :: ClusterId -> ClusterId -> ClusterGraph -> [River]
getRivers ci1 ci2 cg = case M.lookup ci2 (_sc_neighbors $ safeLookup "getRivers" undefined ci1 (_cg_clusters cg)) of
                         Just neighs -> map or2r $ S.toList neighs
                         Nothing -> []

-- | Returns Sites comprising cluster
cgSites :: ClusterId -> ClusterGraph -> Set SiteId
cgSites ci cg = fromMaybe (assert "cgSities" S.empty) $ _sc_sites <$> M.lookup ci (_cg_clusters cg)


-- | O(n) Returns list of clusters with mines, sorted by the number of mines, number of sites
clustersByMines :: ClusterGraph -> MaxPQueue (Int,Int) ClusterId
clustersByMines cg@ClusterGraph{..} =
  L.foldl' f MaxPQueue.empty (zip mcids mclusters)
  where
    f acc (clusterid,SiteCluster{..}) =
      case M.null  _sc_neighbors of
        True -> acc
        False -> MaxPQueue.insert (S.size _sc_mines, S.size _sc_sites) clusterid acc
    mcids = getMines cg
    mclusters = map (_cg_clusters M.!) mcids

-- | Return minimal river of a cluster
-- FIXME implement a better criteria
someClusterRiver :: ClusterGraph -> ClusterId -> Maybe River
someClusterRiver ClusterGraph{..} clid =
  let
    cluster = M.lookup clid _cg_clusters
    rivers = S.unions <$> map snd <$> M.toList <$> _sc_neighbors <$> cluster -- all rivers of a cluster
  in
  or2r <$> S.findMin <$> rivers

-- | Calculates immediate score grow resulting from cluster merge
estimateMergeClusters :: ClusterId -> ClusterId -> ClusterGraph -> Int
estimateMergeClusters cid1 cid2 ClusterGraph{..} =
  case (M.lookup cid1 _cg_scoremaps, M.lookup cid1 _cg_scoremaps) of
    (Just sm1, Just sm2) -> look cid1 sm2 + look cid2 sm1
    _ -> 0
  where
    look k m = M.lookupDefault 0 k m

estimateMergeHRiver :: HyperRiver -> ClusterGraph -> Int
estimateMergeHRiver (HyperRiver c1 c2) = estimateMergeClusters c1 c2

-- MODIFIERS

idx name = Lens.lens get set where
  get m = case M.lookup name m of
            Just x -> x
            Nothing -> error $ "Key " <> show name <> " not found in map"
  set = (\hs mhv -> M.insert name mhv hs)

-- | Removes River 
removeRiver :: River -> ClusterGraph -> ClusterGraph
removeRiver r cg | ci1 == ci2 = cg -- nothing to remove if river is inside the cluster
                 | True = cg'
  where
    riv@(ORiver s1 s2) = mkORiver r
    ci1 = safeLookup "ci1" invalidClusterId s1 (_cg_scmap cg)
    ci2 = safeLookup "ci2" invalidClusterId s2 (_cg_scmap cg)

    cn1,cn2 :: Lens' ClusterGraph CNeighMap
    cn1 = cg_clusters.(idx ci1).sc_neighbors -- map of neighbor rivers of 1
    cn2 = cg_clusters.(idx ci2).sc_neighbors -- map of neighbor rivers of 2

    l_n1 = cn1.(idx ci2) -- set of connections to ci2
    l_n2 = cn2.(idx ci1) -- set of connections to ci1

    n1' = S.delete riv (Lens.view l_n1 cg)
    n2' = S.delete riv (Lens.view l_n2 cg)

    ns1 | S.null n1' = M.delete ci2 (Lens.view cn1 cg)
        | True = M.insert ci2 n1' (Lens.view cn1 cg)

    ns2 | S.null n2' = M.delete ci1 (Lens.view cn2 cg)
        | True = M.insert ci1 n2' (Lens.view cn2 cg)

    cg' = Lens.set cn1 ns1 $
          Lens.set cn2 ns2 $ cg

-- | Merges two neighbouring cluster into one, used when we claimed the river
mergeClusters :: ClusterId -> ClusterId -> ClusterGraph -> ClusterGraph
mergeClusters ci1 ci2 cg | not $ M.member ci2 (_sc_neighbors c1) = error "shouldn't merge disconnected cluster"
                         | True = ClusterGraph { _cg_clusters = clusters''
                                               , _cg_scoremaps = scoremaps''
                                               , _cg_scmap = scmap''
                                               }
  where clusters = _cg_clusters cg
        c1 = clusters M.! ci1
        c2 = clusters M.! ci2
        -- L stands still and R is being renamed
        (ciL, ciR) = if S.size (_sc_sites c1) > S.size (_sc_sites c2) then (ci2,ci1) else (ci1,ci2)
        ni_rest = filter (/= ciL) $ M.keys $ _sc_neighbors $ safeLookup "ni_rest" invalidCluster ciR clusters
        -- patching third party clusters so they point to L
        clusters' = foldr (\ciN clust ->
                              let
                                b = safeLookup "b" invalidCluster ciN clust
                              in
                                M.insert ciN (over sc_neighbors (\nn ->
                                  let
                                    rn = safeLookup "rn" mempty ciR nn
                                  in
                                    M.delete ciR $ M.insertWith S.union ciL rn nn) b) clust)
                    clusters ni_rest
        -- merging R in to L
        cL = fromMaybe (assert "cL" invalidCluster) $ M.lookup ciL clusters
        cR = fromMaybe (assert "cR" invalidCluster) $ M.lookup ciR clusters
        cM = SiteCluster { _sc_sites = S.union (_sc_sites cL) (_sc_sites cR)
                         , _sc_mines = S.union (_sc_mines cL) (_sc_mines cR) -- FIXME better combine RankMaps
                         , _sc_neighbors = M.delete ciR $ M.delete ciL $ M.unionWith (S.union) (_sc_neighbors cL) (_sc_neighbors cR)
                         }

        clusters'' = M.delete ciR $ M.insert ciL cM clusters'
        scmap'' = foldr (\si scm -> M.insert si ciL scm) (_cg_scmap cg) (S.toList $ _sc_sites cR)

        scoremaps' = mergeHashKeys (M.unionWith (+)) ciL ciR (_cg_scoremaps cg) -- combine maps of merging clusters
        scoremaps'' = M.map (mergeHashKeys (+) ciL ciR) scoremaps' -- combine cluster score on each map

-- | Combines value at ksrc into kdst, deletes the former one
mergeHashKeys :: (Eq k, Hashable k) => (v->v->v) -> k -> k -> M.HashMap k v -> M.HashMap k v
mergeHashKeys f kdst ksrc m = M.delete ksrc $ case (vdst, vsrc) of
                                                (Just vd, Just vs) -> M.insert kdst (f vd vs) m
                                                (Just vd, _) -> m
                                                (_, Just vs) -> M.insert kdst vs m
                                                _ -> m
  where
    vsrc = M.lookup ksrc m
    vdst = M.lookup kdst m

mergeClustersByRiver :: ClusterGraph -> River -> ClusterGraph
mergeClustersByRiver cg@ClusterGraph{..} River{..} =
  let
    c1 = _cg_scmap M.! river_source
    c2 = _cg_scmap M.! river_target
  in
  mergeClusters c1 c2 cg

-- CONSTRUCTOR

buildClusterGraph :: SiteMap -> ClusterGraph
buildClusterGraph sm = buildClusterGraphInner sg mines
  where
    sg = buildSiteGraph sm
    mines = V.toList $ map_mines sm



buildClusterGraphInner :: SiteGraph -> [SiteId] -> ClusterGraph
buildClusterGraphInner sg mines = ClusterGraph { _cg_clusters = clusts  -- :: ClusterMap
                                               , _cg_scoremaps = scoremaps
                                               , _cg_scmap = scmap
                                               }
  where
    rmap2score = M.fromList . map (\(k,v)->(site2cluster k, v*v)) . M.toList . unRankMap
    scoremaps = M.fromList $ map (\m -> (site2cluster m, rmap2score $ rankBySite m sg)) mines
    scmap = M.fromList $ map (\s->(s, site2cluster s)) $ M.keys sg
    clusts = M.fromList $ map (\(s,ss) -> (site2cluster s, s2sc s ss)) $ M.toList sg
    s2sc s ss = SiteCluster { _sc_sites = S.singleton s
                            , _sc_mines = if ss_isMine ss then S.singleton s else S.empty
                            , _sc_neighbors = M.fromList $ map (convert_neighs s) $ S.toList $ ss_neighbors ss
                            }
    convert_neighs s n = (site2cluster n, S.singleton (mkORiver (River s n)))
