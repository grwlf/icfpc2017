{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE RecordWildCards      #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Algo.Ranking where

import           Control.Arrow         ((&&&))
import           Control.Monad.Plus
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.HashMap.Strict   (HashMap)
import qualified Data.HashMap.Strict   as M
import           Data.List             (foldl', sortBy)
import           Data.Ord              (Down (..), comparing)
import qualified Data.Set              as S
import           System.Random.Shuffle

import           Imports               hiding (StateT, runStateT)
import           SiteGraph
import           Types

type Rank       = Int
newtype RankMap = RankMap { unRankMap :: HashMap SiteId Int }
                deriving (Show, Generic, Binary)

getRank :: SiteId -> RankMap -> Int
getRank sid = (M.! sid) . unRankMap

--------------------------------------------------------------------------------

rankBySite :: SiteId -> SiteGraph -> RankMap
rankBySite n = runReader $ rankRecursion [n] (0, RankMap $ M.singleton n 0)

rankRecursion :: [SiteId] -> (Rank, RankMap) -> Reader SiteGraph RankMap
rankRecursion [] st = return $ snd st
rankRecursion ns st = runStateT (rankNeighbors ns) st >>= uncurry rankRecursion

rankNeighbors :: [SiteId] -> StateT (Rank, RankMap) (Reader SiteGraph) [SiteId]
rankNeighbors ns = do
  g         <- lift ask
  (r, rmap) <- get
  let rmap' = foldr (`M.insert` r) (unRankMap rmap) ns
      nns   = S.toList . S.unions . map (S.filter (not . flip M.member rmap') .
                                         ss_neighbors . (g M.!)) $ ns
  put (r + 1, RankMap rmap')
  return nns

--------------------------------------------------------------------------------

data RankData = RankData { rGraph   :: SiteGraph
                         , rMines   :: [SiteId]
                         , rMaps    :: [RankMap]
                         , rRiver   :: [SiteId]
                         , rLambdas :: [SiteId]
                         , rFinMap  :: RankMap
                         , rConnect :: [SiteId]
                         }
              deriving (Show, Binary, Generic)

buildRankData1 :: SiteGraph -> IO RankData
buildRankData1 g = do
  mines <- shuffleM <$> map fst . filter (ss_isMine . snd) . M.toList $ g
  return RankData { rGraph   = g
                  , rMines   = mines
                  , rMaps    = map (`rankBySite` g) mines
                  , rRiver   = []
                  , rLambdas = []
                  , rFinMap  = RankMap M.empty
                  , rConnect = []
                  }

getRiver1 :: RankData -> IO (Maybe River, RankData)
getRiver1 rdata = msum [ continueRiver rdata
                       , connectToMine rdata
                       , randomWalk rdata
                       , return (Nothing, rdata)
                       ]

continueRiver :: RankData -> IO (Maybe River, RankData)
continueRiver rdata = do
  river <- mfromMaybe $ findRiverByRank rdata
  let to     = river_target river
      rdata' = rdata { rRiver   = to : rRiver rdata
                     , rLambdas = maybeAddMine to
                     }
  return (Just river, rdata')
  where g     = rGraph rdata
        mines = rLambdas rdata

        maybeAddMine n | ss_isMine $ g M.! n = n : mines
                       | otherwise           = mines

findRiverByRank :: RankData -> Maybe River
findRiverByRank RankData {..}
  | null rRiver = Nothing
  | null ns     = Nothing
  | otherwise   = Just River { river_source = from
                             , river_target = head sortedSites
                             }
  where from        = head rRiver
        ns          = S.toList . ss_neighbors $ rGraph M.! from
        calcRanks n = foldl' (\r rmap -> r + (rmap M.! n) ^ (2::Int)) 0 .
                      map unRankMap $ rMaps
        sortedSites = map fst . sortBy (comparing (Down . snd)) $
                      map (id &&& calcRanks) ns

connectToMine :: RankData -> IO (Maybe River, RankData)
connectToMine rdata = do
  guard . not . null $ nsList
  to <- (ns !!) <$> randomRIO (0, length ns - 1)
  let river  = River { river_source = from
                     , river_target = to
                     }
      rdata' = rdata { rRiver   = [to, from]
                     , rLambdas = [from]
                     }
  return (Just river, rdata')
  where getNs      = S.toList . ss_neighbors . (M.!) (rGraph rdata)
        nsList     = dropWhile (null . snd) . map (id &&& getNs) $
                     rMines rdata
        (from, ns) = head nsList

randomWalk :: RankData -> IO (Maybe River, RankData)
randomWalk rdata = do
  sites <- shuffleM <$> map fst . M.toList $ g
  let nsList     = dropWhile (null . snd) . map (id &&& getNs) $ sites
      (from, ns) = head nsList
  guard . not . null $ nsList
  to <- (ns !!) <$> randomRIO (0, length ns - 1)
  let river = River { river_source = from
                    , river_target = to
                    }
  return (Just river, rdata)
  where g          = rGraph rdata
        getNs      = S.toList . ss_neighbors . (M.!) g

--------------------------------------------------------------------------------

buildRankData2 :: SiteGraph -> IO RankData
buildRankData2 g = return RankData { rGraph   = g
                                   , rMines   = mines'
                                   , rMaps    = ranks'
                                   , rRiver   = [mine']
                                   , rLambdas = [mine']
                                   , rFinMap  = rankBySite finish g
                                   , rConnect = []
                                   }
  where mines            = map fst . filter (ss_isMine . snd) . M.toList $ g
        ranks            = map (`rankBySite` g) mines
        finish           = findMinByRanks g ranks
        (mines', ranks') = unzip . sortBy (comparing $ (M.! finish) .
                                           unRankMap . snd) $ zip mines ranks
        mine'            = head mines'

findMinByRanks :: SiteGraph -> [RankMap] -> SiteId
findMinByRanks g xs = minimumBy (compare `on` getRankSum) .
                      map fst . M.toList $ g
  where getRankSum :: SiteId -> Int
        getRankSum sid = foldl' (\r rmap -> r + (unRankMap rmap M.! sid)) 0 xs

getRiver2 :: RankData -> IO (Maybe River, RankData)
getRiver2 rdata = msum [ startFromMine rdata
                       , walkFromLastMine rdata
                       , randomWalk rdata
                       , return (Nothing, rdata)
                       ]

startFromMine :: RankData -> IO (Maybe River, RankData)
startFromMine rdata
  | null $ rLambdas rdata
  = mzero
  | rank == 0 || null ns'
  = case () of
      _ | from == mine && null mtail
          -> startFromMine rdata { rRiver   = tail (rRiver rdata)
                                 , rLambdas = tail (rLambdas rdata)
                                 }
      _ | from == mine
          -> startFromMine rdata { rRiver   = mnext : tail (rRiver rdata)
                                 , rLambdas = mnext : tail (rLambdas rdata)
                                 }
      _ | null mtail
          -> mzero
      _   -> startFromMine rdata { rRiver   = mnext : rRiver rdata
                                 , rLambdas = mnext : rLambdas rdata
                                 }
  | otherwise
  = return (Just river, rdata' { rRiver = to : rRiver rdata })
  where from  = head $ rRiver rdata
        mine  = head $ rLambdas rdata
        ns    = S.toList . ss_neighbors $ rGraph rdata M.! from
        rmap  = rFinMap rdata
        rank  = unRankMap rmap M.! from
        ns'   = map snd . sortBy (comparing fst) . filter ((<= rank) . fst) $
                zip (map (unRankMap rmap M.!) ns) ns
        mtail = tail . dropWhile (/= mine) $ rMines rdata
        mnext = head mtail
        to    = head ns'

        river  = River { river_source = from, river_target = to }
        rdata' = if unRankMap rmap M.! to == 0
                 then rdata { rConnect = mine : rConnect rdata }
                 else rdata

walkFromLastMine :: RankData -> IO (Maybe River, RankData)
walkFromLastMine rd
  | null (rLambdas rd) = continueWalk rd
  | otherwise          = attachToMine rd { rLambdas = [] }
  where
    g    = rGraph rd
    getN = S.toList . ss_neighbors . (g M.!)

    attachToMine rdata
      | null conn = mzero
      | otherwise = do
          to <- (ns !!) <$> randomRIO (0, length ns - 1)
          let from   = head conn
              river  = River { river_source = from, river_target = to }
              rdata' = rdata { rRiver   = [to, from]
                             , rConnect = conn
                             }
          return (Just river, rdata')
      where (conn, ~(ns:_)) = unzip . dropWhile (null . snd) .
                              map (id &&& getN) $ rConnect rdata

    continueWalk rdata
      | null rs || null ns' = if null conn
                              then mzero
                              else attachToMine rdata { rConnect = tail conn }
      | otherwise           = return (Just river, rdata')
      where conn          = rConnect rdata
            (rs, ~(ns:_)) = unzip . dropWhile (null . snd) .
                            map (id &&& getN) $ rRiver rdata
            from          = head rs
            rmap          = snd . fromJust . find ((== head conn) . fst) $
                            zip (rMines rdata) (rMaps rdata)
            rank          = unRankMap rmap M.! from
            ns'           = map snd . sortBy (comparing fst) .
                            filter ((>= rank) . fst) $
                            zip (map (unRankMap rmap M.!) ns) ns
            to            = head ns'

            river  = River { river_source = from, river_target = to }
            rdata' = rdata { rRiver = to : rRiver rdata }
