{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE DeriveGeneric        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE RecordWildCards      #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Algo.RankMap where

import           Control.Arrow         ((&&&))
import           Control.Monad.Reader
import           Control.Monad.State
import qualified Data.Binary           as Binary
import           Data.HashMap.Strict   (HashMap)
import qualified Data.HashMap.Strict   as M
import           Data.List             (foldl', sortBy)
import           Data.Ord              (Down (..), comparing)
import qualified Data.Set              as S

import           Imports               hiding (StateT, runStateT)
import           SiteGraph
import           Types

type Rank       = Int
newtype RankMap = RankMap { unRankMap :: HashMap SiteId Int }
                deriving (Show,Generic,Binary)

getRank :: SiteId -> RankMap -> Int
getRank sid = fromMaybe (error "getRank") . M.lookup sid . unRankMap

--------------------------------------------------------------------------------

rankBySite :: SiteId -> SiteGraph -> RankMap
rankBySite n = runReader $ rankRecursion [n] (0, RankMap $ M.singleton n 0)

rankRecursion :: [SiteId] -> (Rank, RankMap) -> Reader SiteGraph RankMap
rankRecursion [] st = return $ snd st
rankRecursion ns st = runStateT (rankNeighbors ns) st >>= uncurry rankRecursion

rankNeighbors :: [SiteId] -> StateT (Rank, RankMap) (Reader SiteGraph) [SiteId]
rankNeighbors ns = do
  g         <- lift ask
  (r, rmap) <- get
  let rmap' = foldr (`M.insert` r) (unRankMap rmap) ns
      nns   = S.toList . S.unions . map (S.filter (not . flip M.member rmap') .
                                         ss_neighbors .
                                         (\x -> fromMaybe (error "rankNei") $ M.lookup x g)) $ ns
  put (r + 1, RankMap rmap')
  return nns

