module Algo.AStar where

import Control.Applicative
import Data.Graph.AStar
import Data.HashSet as S

import Algo.ClusterGraph

cgAStar :: ClusterGraph -> ClusterId -> [ClusterId] -> Maybe [ClusterId]
cgAStar cg ci1 tgts = (ci1:) <$> aStar (S.fromList . flip getNeighs cg)
                                       (\_ _ -> 1)
                                       (const 0)
                                       (\p -> S.member p (S.fromList tgts))
                                       ci1
