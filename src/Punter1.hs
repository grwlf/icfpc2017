{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE NondecreasingIndentation #-}

module Punter1 where

import System.IO

import Algo.Ranking
import Imports
import Types
import SiteGraph

data PunterState a = PunterState {
    st_pid :: PunterId
  , st_map :: a
  }
  deriving (Show,Generic,Binary)

punterF_gen :: (SiteMap -> IO a) -> PunterId -> SiteMap -> IO (PunterState a)
punterF_gen mf pid smap = do
  x <- mf smap
  return PunterState { st_pid = pid
                     , st_map = x
                     }

punterF :: UTCTime -> (a -> SiteGraph)
        -> (a -> SiteGraph -> IO (Maybe River, a))
        -> Gameplay_Req_S (PunterState a) -> IO (Gameplay_Res_S (PunterState a))
punterF tstart getGr update gameplay@Gameplay_Req_S{} = do
  let ps = gp_req_state gameplay
      st = st_map ps
      g' = foldr removeMove (getGr st) $ gp_req_moves_s gameplay

  (river, st') <- update st g'

  dt <- diffTimeFrom tstart
  hPutStrLn stderr $ "time " <> show dt <> " pid " <> show (st_pid ps)

  return Gameplay_Res_S { gp_res_move_s = toMove (st_pid ps) river
                        , gp_res_state  = ps { st_map = st' }
                        }
  where removeMove :: Move -> SiteGraph -> SiteGraph
        removeMove mv = case fromMove mv of
                          Nothing -> id
                          Just rv -> removeRiver rv

        fromMove :: Move -> Maybe River
        fromMove (MovePass _)   = Nothing
        fromMove (MoveClaim mv) = Just River { river_source = claim_source mv
                                             , river_target = claim_target mv
                                             }

        toMove :: PunterId -> Maybe River -> Move
        toMove p Nothing      = MovePass Pass
                                { pass_punter = p
                                }
        toMove p (Just river) = MoveClaim Claim
                                { claim_punter = p
                                , claim_source = river_source river
                                , claim_target = river_target river
                                }

punter1_gen :: PunterId -> SiteMap -> Extensions -> IO (PunterState RankData)
punter1_gen a b _ = punterF_gen (buildRankData2 . buildSiteGraph) a b

punter1 :: UTCTime -> Gameplay_Req_S (PunterState RankData)
        -> IO (Gameplay_Res_S (PunterState RankData))
punter1 tstart = punterF tstart rGraph (\st g -> getRiver2 st { rGraph = g })
