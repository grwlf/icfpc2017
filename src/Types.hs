{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Types where

import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Types as Aeson
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Vector as Vector
import qualified Data.Binary as Binary

import Control.DeepSeq (NFData)
import Imports

data Site = Site {
    site_id :: Int
  , site_x :: Float
  , site_y :: Float
  } deriving(Read,Show, Eq,Ord,Generic,Binary,NFData)

-- instance Show Site where
--   show Site{..} = concat [show site_id, "(", show site_x, ",", show site_y, ")"]

printSite Site{..} = concat [show site_id, "(", show site_x, ",", show site_y, ")"]

instance FromJSON Site where
  parseJSON = Aeson.withObject "site" $ \o ->
    Site
      <$> o .: "id"
      <*> (fromMaybe 0.0 <$> (o .:? "x"))
      <*> (fromMaybe 0.0 <$> (o .:? "y"))

instance ToJSON Site where
  toJSON s =
    Aeson.object [ "id" .= site_id s
                 , "x" .= site_x s
                 , "y" .= site_y s
                 ]

safeFromJust :: Maybe Int -> Aeson.Parser Int
safeFromJust (Just x) = return x
safeFromJust Nothing = fail "Failed to parse SiteId or RiverId"

newtype SiteId = SiteId { s_id :: Int }
  deriving(Show,Read,Eq,Ord,Generic,Hashable,Binary,NFData)

siteId :: Site -> SiteId
siteId s = SiteId (site_id s)

invalidSiteId = SiteId (-1)

-- instance Show SiteId where
--   show (SiteId s) = "S-" ++ show s

printSiteId (SiteId s) = "S" <> show s

instance FromJSON SiteId where
  parseJSON (Aeson.Number x) = SiteId <$> safeFromJust (toBoundedInteger x)
instance FromJSONKey SiteId

instance ToJSON SiteId where
  toJSON (SiteId sid) = Aeson.toJSON sid

newtype RiverId = RiverId { r_id :: Int }
  deriving(Read,Show,Eq,Ord,Generic,Hashable,Binary,NFData)

instance ToJSON RiverId where
  toJSON (RiverId rid) = Aeson.toJSON rid


instance FromJSON RiverId where
  parseJSON (Aeson.Number x) = RiverId <$> safeFromJust (toBoundedInteger x)
instance FromJSONKey RiverId

type Sites = HashMap SiteId Site
type Rivers = Vector River
type Mines = Vector SiteId

instance (Eq a, Hashable a, Binary a, Binary b) => Binary (HashMap a b) where
  put x = Binary.put $ HashMap.toList x
  get = HashMap.fromList <$> Binary.get

instance (Binary a) => Binary (Vector a) where
  put x = Binary.put $ Vector.toList x
  get = Vector.fromList <$> Binary.get

data River = River {
    river_source :: SiteId
  , river_target :: SiteId
  } deriving(Read,Show,Eq,Ord,Generic,Binary,NFData)

-- instance Show River where
--   show (River (SiteId s) (SiteId t)) = concat ["R", show s, "-", show t]

printRiver (River (SiteId s) (SiteId t)) = concat ["R", show s, "-", show t]

instance FromJSON River where
  parseJSON = Aeson.withObject "river" $ \o ->
    River <$> (SiteId <$> o .: "source")
          <*> (SiteId <$> o .: "target")

data SiteMap = SiteMap {
    map_sites :: Sites
  , map_rivers :: Rivers
  , map_mines :: Mines
  } deriving(Read,Show,Eq,Generic,Binary,NFData)

map_site :: SiteId -> SiteMap -> Site
map_site sid SiteMap{..} = fromJust $ HashMap.lookup sid map_sites

instance FromJSON SiteMap where
  parseJSON = Aeson.withObject "sitemap" $ \o ->
    SiteMap
      <$> (let vec2hash v = HashMap.fromList $ map (\s -> (SiteId $ site_id s, s)) $ Vector.toList v in liftA vec2hash $ o .: "sites")
      <*> (o .: "rivers")
      <*> (o .: "mines")

data Handshake = Handshake { handshake_name :: Text }
  deriving(Read,Show,Eq,Ord,Generic)


instance FromJSON Handshake where
  parseJSON (Aeson.Object o) =
    Handshake <$> o .: "you"

instance ToJSON Handshake where
  toJSON x = Aeson.object [
      "me" .= handshake_name x
    ]


data PunterId = PunterId { p_id :: Int }
  deriving(Read,Show,Eq,Ord,Generic,Binary,NFData)

instance FromJSON PunterId where
  parseJSON (Aeson.Number x) = PunterId <$> safeFromJust (toBoundedInteger x)

instance ToJSON PunterId where
  toJSON (PunterId x) = Aeson.toJSON x

data Settings = Settings {
    settings_futures :: Maybe Bool
  , settings_options :: Maybe Bool
  } deriving(Read,Show,Eq,Generic)

-- Final value for extensions, filled by the Client.hs
data Extensions = Extensions {
    ext_options :: Bool
  } deriving(Show,Generic,Binary,NFData)

instance FromJSON Settings where
  parseJSON = Aeson.withObject "settings" $ \o ->
    Settings
      <$> o .:? "futures"
      <*> o .:? "options"

-- S -> P
data Setup_Req = Setup_Req {
    setup_req_punterid :: PunterId
  , setup_req_punters :: Int
  , setup_req_map :: SiteMap
  , setup_req_settings :: Maybe Settings
  }
  deriving(Read,Show,Eq,Generic)

hasOptions Setup_Req{..} = fromMaybe False $ setup_req_settings >>= settings_options
hasFutures Setup_Req{..} = fromMaybe False $ setup_req_settings >>= settings_futures

instance FromJSON Setup_Req where
  parseJSON = Aeson.withObject "req" $ \o ->
    Setup_Req
      <$> o .: "punter"
      <*> o .: "punters"
      <*> o .: "map"
      <*> o .:? "settings"

data Setup_Res = Setup_Res {
    setup_res_punterid :: PunterId
  }
  deriving(Read,Show,Eq,Generic)

data Setup_Res_S s = Setup_Res_S {
    setup_res_punterid_s :: PunterId
  , setup_res_state :: s -- () is used for online mode
  }
  deriving(Read,Show,Eq,Generic)

instance ToJSON Setup_Res where
  toJSON x = Aeson.object [
      "ready" .= setup_res_punterid x
    ]

instance ToJSON s => ToJSON (Setup_Res_S s) where
  toJSON x = Aeson.object [
      "ready" .= setup_res_punterid_s x
    , "state" .= setup_res_state x
    ]


data Claim = Claim {
    claim_punter :: PunterId
  , claim_source :: SiteId
  , claim_target :: SiteId
  } deriving(Read,Show,Eq,Generic)

instance FromJSON Claim where
  parseJSON = Aeson.withObject "claim" $ \o ->
    Claim <$> (o .: "punter") <*> (o.:"source") <*> (o.:"target")

instance ToJSON Claim where
  toJSON Claim{..} = Aeson.object [ "punter" .= Aeson.toJSON claim_punter
                                  , "source" .= Aeson.toJSON claim_source
                                  , "target" .= Aeson.toJSON claim_target
                                  ]

data Pass = Pass {
    pass_punter :: PunterId
  } deriving(Read,Show,Eq,Generic)

instance FromJSON Pass where
  parseJSON = Aeson.withObject "pass" $ \o ->
    Pass <$> (o .: "punter")

instance ToJSON Pass where
  toJSON (Pass pid) = Aeson.object [ "punter" .= Aeson.toJSON pid]


data Splurge = Splurge {
    spl_punter :: PunterId
  , spl_route :: [SiteId]
  } deriving(Read,Show,Eq,Generic)

instance FromJSON Splurge where
  parseJSON = Aeson.withObject "splurge" $ \o ->
    Splurge <$> (o .: "punter")
            <*> (o .: "route")

data Move = MoveClaim Claim | MovePass Pass | MoveOption Claim | MoveSplurge Splurge | MoveUnknown
  deriving(Read,Show,Eq,Generic)

instance FromJSON Move where
  parseJSON = Aeson.withObject "move" $ \o ->
    (MoveClaim <$> (o .: "claim")) <|> (MovePass <$> (o .: "pass"))
               <|> (MoveOption <$> (o .: "option")) <|> (MoveSplurge <$> (o .: "splurge")) <|> pure MoveUnknown

printMove :: Move -> String
printMove (MoveClaim Claim {..}) = "punter " <> show (p_id claim_punter) <> " claim " <> printSiteId claim_source <> ".." <> printSiteId claim_target
printMove (MovePass Pass {..}) = "punter " <> show (p_id pass_punter) <> " pass"
printMove (MoveOption Claim {..}) = "punter " <> show (p_id claim_punter) <> " option " <> printSiteId claim_source <> ".." <> printSiteId claim_target
printMove (MoveSplurge Splurge{..}) = "punter" <> show (p_id spl_punter) <> " path " <> show spl_route

movePass :: PunterId -> Move
movePass pid = MovePass (Pass pid)

moveClaim :: PunterId -> SiteId -> SiteId -> Move
moveClaim pid f t = MoveClaim (Claim pid f t)

claimRiver :: PunterId -> River -> Move
claimRiver pid River{..} = MoveClaim (Claim pid river_source river_target)

optionRiver :: PunterId -> River -> Move
optionRiver pid River{..} = MoveOption (Claim pid river_source river_target)

data Gameplay_Req = Gameplay_Req {
    gp_req_moves :: [Move]
  } deriving(Read,Show,Eq,Generic)

-- the river claimed by us, rivers claimed by others
claimedRivers :: PunterId -> [Move] -> ([River], [River])
claimedRivers pid = foldl' f ([],[]) where
  f (us,others) (MoveClaim Claim{..}) =
    case claim_punter == pid of
      True  -> ((River claim_source claim_target):us, others)
      False -> (us, (River claim_source claim_target):others)
  f (us,others) (MoveSplurge Splurge{..}) =
    let
      link_route [] = []
      link_route [x] = []
      link_route (s1:s2:r) = (River s1 s2):(link_route (s2:r))
    in
    case spl_punter == pid of
      True  -> ((link_route spl_route)<>us, others)
      False -> (us, (link_route spl_route)<>others)
  f acc _ = acc

data Gameplay_Req_S s = Gameplay_Req_S {
    gp_req_moves_s :: [Move]
  , gp_req_state :: s
  } deriving(Read,Show,Eq,Generic)

instance FromJSON Gameplay_Req where
  parseJSON =
      Aeson.withObject "gamplay_req" (\o ->
        Gameplay_Req
          <$> ((o.: "move") >>= (.: "moves"))
      )

instance (FromJSON s) => FromJSON (Gameplay_Req_S s) where
  parseJSON =
      Aeson.withObject "gamplay_req" (\o ->
        Gameplay_Req_S
          <$> ((o.: "move") >>= (.: "moves"))
          <*> (o.:"state")
    )

-- type Gameplay_Res = Move
data Gameplay_Res = Gameplay_Res {
    gp_res_move :: Move
  } deriving(Read,Show,Eq,Generic)

data Gameplay_Res_S s = Gameplay_Res_S {
    gp_res_move_s :: Move
  , gp_res_state :: s
  } deriving(Read,Show,Eq,Generic)

instance ToJSON (Gameplay_Res) where
  toJSON (Gameplay_Res (MoveClaim cl)) =
    Aeson.object [ "claim" .= (Aeson.toJSON cl) ]
  toJSON (Gameplay_Res (MovePass pa)) =
    Aeson.object [ "pass" .= (Aeson.toJSON pa) ]
  toJSON (Gameplay_Res (MoveOption cl)) =
    Aeson.object [ "option" .= (Aeson.toJSON cl) ]

instance ToJSON s => ToJSON (Gameplay_Res_S s) where
  toJSON (Gameplay_Res_S (MoveClaim cl) s) =
    Aeson.object [ "claim" .= (Aeson.toJSON cl), "state" .= (Aeson.toJSON s) ]
  toJSON (Gameplay_Res_S (MovePass pa) s) =
    Aeson.object [ "pass" .= (Aeson.toJSON pa) , "state" .= (Aeson.toJSON s) ]
  toJSON (Gameplay_Res_S (MoveOption cl) s) =
    Aeson.object [ "option" .= (Aeson.toJSON cl), "state" .= (Aeson.toJSON s) ]


data Score = Score {
    score_punter :: PunterId
  , score_scores :: Int
  } deriving(Read,Show,Eq,Generic)

instance FromJSON Score where
  parseJSON = Aeson.withObject "score" $ \o ->
    Score <$> o.: "punter" <*> o.:"score"

data Scoring_Req = Scoring_Req {
    scoring_move :: [Move]
  , scoring_scores :: [Score]
  } deriving(Read,Show,Eq,Generic)

scoresGain :: PunterId -> [Score] -> (Maybe Int, [Int])
scoresGain pid scores =
  let
    sc = map (\(Score pid sc) -> (pid,sc)) scores
  in
  (lookup pid sc, reverse $ sort $ map snd $ filter (\(p,_) -> p/=pid) sc)

instance FromJSON Scoring_Req where
  parseJSON = Aeson.withObject "scoring_req" $ \o ->
      (o.: "stop") >>= (\o2 ->
        Scoring_Req
          <$> o2 .: "moves"
          <*> o2 .: "scores"
          )


data Timeout = Timeout {
  timout_len :: Float
  }
  deriving(Read,Show,Eq,Generic)

instance FromJSON Timeout where
  parseJSON = Aeson.withObject "timeout" $ \o ->
    Timeout <$> o.: "timeout"


data Req = ReqTimeout Timeout
         | ReqScoring Scoring_Req
         | ReqGameplay Gameplay_Req
  deriving(Read,Show,Eq,Generic)

data Req_S s = ReqTimeout_S Timeout
             | ReqSetup_S Setup_Req
             | ReqGameplay_S (Gameplay_Req_S s)
             | ReqScoring_S Scoring_Req
  deriving(Read,Show,Eq,Generic)

instance FromJSON Req where
  parseJSON o =
    (ReqTimeout <$> (Aeson.parseJSON o))
    <|>
    (ReqScoring <$> (Aeson.parseJSON o))
    <|>
    (ReqGameplay <$> (Aeson.parseJSON o))

instance (FromJSON s) => FromJSON (Req_S s) where
  parseJSON o =
    (ReqTimeout_S <$> (Aeson.parseJSON o))
    <|>
    (ReqSetup_S <$> (Aeson.parseJSON o))
    <|>
    (ReqScoring_S <$> (Aeson.parseJSON o))
    <|>
    (ReqGameplay_S <$> (Aeson.parseJSON o))

