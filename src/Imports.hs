{-# LANGUAGE UndecidableInstances #-}

module Imports (
    module Control.Arrow
  , module Control.Applicative
  , module Control.Concurrent
  , module Control.Concurrent.STM
  , module Control.Monad
  , module Control.Monad.Trans
  , module Control.Monad.State.Strict
  , module Control.Monad.Random
  -- , module Control.Monad.Rnd
  , module Control.Monad.Reader
  , module Control.Lens
  , module Control.Exception
  -- , module Control.Monad.Loops
  -- , module Control.Break_RL
  , module Data.Aeson
  , module Data.Bits
  , module Data.Data
  , module Data.ByteString.Char8
  , module Data.Binary
  , module Data.Typeable
  , module Data.Ratio
  , module Data.Tuple
  , module Data.List
  , module Data.Map.Strict
  , module Data.HashMap.Strict
  , module Data.HashSet
  , module Data.Maybe
  , module Data.Set
  , module Data.Function
  , module Data.Foldable
  , module Data.Text
  , module Data.Monoid
  , module Data.Hashable
  , module Data.Scientific
  , module Data.Vector
  , module Data.Time
  , module Debug.Trace
  , module Prelude
  , module System.Random
  , module System.Random.Mersenne.Pure64
  , module System.Directory
  , module System.FilePath
  , module Text.Printf
  , module Text.Heredoc_RL
  , module Text.Show.Pretty
  -- , module Graphics.TinyPlot
  , module GHC.Generics
  , module Imports
)

where

import Control.Arrow ((&&&),(***))
import Control.Applicative
import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad
import Control.Monad.Trans
import Control.Monad.State.Strict
-- import Control.Monad.Rnd
import Control.Monad.Reader(Reader,runReaderT, MonadReader(..))
import Control.Lens (Lens', makeLenses, (%=), (^.), over, view, use, uses, zoom, _1, _2, _3, _4, _5, _6)
-- import Control.Monad.Loops
-- import Control.Break_RL
import Control.Monad.Trans.Except (ExceptT, runExceptT, throwE)
import Control.Exception (SomeException, catch, evaluate)
import Control.Monad.Random (MonadRandom(..))
import Data.Aeson ((.=), (.:), (.:?), (.!=), FromJSON, ToJSON, FromJSONKey, ToJSONKey)
import Data.Data(Data)
import Data.Typeable(Typeable)
import Data.Bits
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as ByteString
import Data.Ratio
import Data.Tuple
import Data.List hiding (break)
import qualified Data.List as List
import Data.Map.Strict (Map, (!))
import qualified Data.Map.Strict as Map
import Data.Set (Set,member)
import qualified Data.Set as Set
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.HashSet (HashSet)
import Data.Maybe
import Data.Foldable
import Data.Function
import Data.Vector(Vector)
import Data.Scientific
import Debug.Trace hiding(traceM)
import Prelude hiding(break)
import System.Random
import System.FilePath ((</>))
import System.Random.Mersenne.Pure64
import System.Directory
import Text.Printf
import Text.Heredoc_RL
import Text.Show.Pretty
-- import Graphics.TinyPlot
import Data.Text (Text)
import Data.Monoid ((<>))
import Data.Hashable
import Data.Binary hiding(get,put)
import GHC.Generics (Generic)
import Data.PQueue.Prio.Min(MinPQueue)
import qualified Data.PQueue.Prio.Min as MinPQueue
import Data.PQueue.Prio.Max(MaxPQueue)
import qualified Data.PQueue.Prio.Max as MaxPQueue
import Data.Time(diffUTCTime, getCurrentTime,UTCTime,NominalDiffTime)

bpack :: String -> ByteString
bpack = ByteString.pack

bshow :: (Show a) => a -> ByteString
bshow = ByteString.pack . show

bread :: (Read a) => ByteString -> a
bread = read . ByteString.unpack

bunpack :: ByteString -> String
bunpack = ByteString.unpack

trace1 :: (Show a) => a -> a
trace1 a = trace (ppShow a) a

traceM :: (Monad m, Show a) => a -> m ()
traceM a = trace (ppShow a) (return ())

trace' :: (Show a) => a -> b -> b
trace' a b = trace (ppShow a) b

diffTimeFrom :: (MonadIO m) => UTCTime -> m NominalDiffTime
diffTimeFrom tstart = do
  t <- liftIO $ getCurrentTime
  return $ diffUTCTime t tstart

-- loopM s0 f m = iterateUntilM (not . f) m s0

-- iter  :: Monad m => r -> (r -> Break r m r) -> m r
-- iter r0 f = do
--     x <- runExceptT (unBreak (f r0))
--     case x of
--         Left  r -> return r
--         Right r -> iter r f

whileM :: (Monad m) => m Bool -> m ()
whileM m = do
  x <- m
  case x of
    True -> whileM m
    False -> return ()

maxPQView :: (Ord k) => a -> MaxPQueue k a -> a
maxPQView def pq = maybe def fst (MaxPQueue.maxView pq)

