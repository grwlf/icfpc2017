{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE NondecreasingIndentation #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Punter2 where

import qualified Data.PQueue.Prio.Min as MinPQueue
import qualified Data.PQueue.Prio.Max as MaxPQueue
import qualified Data.Vector as V
import qualified Data.Set as S
import qualified Data.HashMap.Strict as M
import qualified Control.Monad.Random as Random
-- import Control.Monad.Random (MonadRandom)

import System.Environment
import Control.DeepSeq
import System.IO
import System.Random
-- import Algo.Ranking
import Algo.ClusterGraph
import Algo.AStar
import Algo.Heuristics
import Imports
import Types hiding (Pass,Claim,Option)

hasEnv :: (MonadIO m) => String -> m Bool
hasEnv str = liftIO $ lookupEnv str >>= return . isJust

randomItem :: (MonadRandom m) => HashMap a b -> m (Maybe b)
randomItem hm = Random.fromListMay $ (map snd $ M.toList hm)`zip`(repeat 1)

randomRiver :: (MonadRandom m) => ClusterGraph -> m (Maybe River)
randomRiver ClusterGraph{..} = do
  ri <- randomItem _cg_clusters
  case ri of
    Just cl -> do
      ri <- randomItem (_sc_neighbors cl)
      case ri of
        Just nei | S.null nei -> return Nothing
                 | otherwise -> return $ Just $ or2r $ S.findMin nei
        Nothing -> return Nothing
    Nothing -> return Nothing


-- FIXME  - try to detect timeouts and cut down the heavy logic in this case
-- FIXME  - experiment with extream conditions i.e. no mines, huge clusters etc
-- FIXME  - use delay and see how offline simulator responds
data PunterState = PunterState {
    _ps_pid :: PunterId
  -- , _ps_sm :: SiteMap
  , _ps_cg :: ClusterGraph
  , _ps_mines :: [SiteId]
  , _ps_recovery :: Bool -- catched timeout/exception on previous move
  , _ps_extensions :: Extensions -- enabled extension
  , _ps_enemy_rivers :: [ORiver]
  , _ps_num_options :: Int
  }
  deriving (Show,Generic,Binary,NFData)

$(makeLenses ''PunterState)

debug :: (MonadIO m) => String -> m ()
debug str = liftIO $ hPutStrLn stderr str

punter_gen :: PunterId -> SiteMap -> Extensions -> IO PunterState
punter_gen pid sm0 ext = tweak sm0 >>= initps where

  tweak = execStateT $ do
    nomines <- hasEnv "PUNTER_NOMINES"
    when nomines $ do
      debug "Stressmode: Wiping all mines"
      modify $ \sm -> sm{map_mines = V.empty}

  initps sm =
    let
      cg = buildClusterGraph sm
      mines = V.toList (map_mines sm)
    in do
    return $ PunterState { _ps_pid = pid
                         , _ps_cg = cg
                         , _ps_mines = mines
                         , _ps_recovery = False
                         , _ps_extensions = ext
                         , _ps_enemy_rivers = []
                         , _ps_num_options = if ext_options ext then length mines else 0
                         }

data Decision = Claim River | Option River | Pass
  deriving (Show, NFData, Generic, Eq)


chooseDecision :: [Decision] -> Decision
chooseDecision [] = Pass
chooseDecision (d:ds) =
  case d of
    Claim _ -> d
    Option _ -> d
    Pass -> chooseDecision ds

chooseDecisionRnd :: (MonadRandom m) => [(Decision, Rational)] -> m Decision
chooseDecisionRnd dec = do
  let dec2 = filter (\(d,w) -> d /= Pass) dec
  d <- Random.fromListMay dec2
  case d of
    Just d -> return d
    Nothing -> return Pass

strategy_option :: ClusterGraph -> StateT PunterState IO Decision
strategy_option cg2 = do
  options <- ext_options <$> use ps_extensions
  no <- use ps_num_options
  case options && no > 0 of
    True -> do
      -- strategy to use option
      use ps_enemy_rivers >>= \ers -> do
        let interesting_ers = filter (\(_, HyperRiver c1 c2) -> c1 /= c2) $ map (\or -> (or, oriver2hriver or cg2))  ers
        let best_ers = sortBy (\x1 x2 -> compare (snd x2) (snd x1)) $ map (\rp -> (rp, estimateMergeHRiver (snd rp) cg2)) $ interesting_ers
        -- TODO: "may claim the first one if snd x if large enough
        ps_enemy_rivers %= const (map fst interesting_ers)

        case best_ers of
          (((or,_),_):_) -> do
            return (Option (or2r or))

          _ -> return Pass

    False -> do
      return Pass

strategy_hotspot :: ClusterGraph -> StateT PunterState IO Decision
strategy_hotspot cg2 = do
  hottest <- liftIO $ hottestRivers cg2
  case hottest of
    ((hr,temper):_) | temper > 3  -> do let rivs = getRivers (hr_c1 hr) (hr_c2 hr) cg2
                                        case rivs of
                                          (r:_)->return $ Claim r
                                          _ -> return $ Pass
    _ -> return Pass

strategy_astar :: ClusterGraph -> StateT PunterState IO Decision
strategy_astar cg2 = do
  clust <- pure $ maxPQView invalidClusterId $ clustersByMines cg2
  river0 <- pure $ someClusterRiver cg2 clust

  clust_mines <-
    filter (/= clust)
      <$> map (\m -> safeLookup "clust_mines" invalidClusterId m (_cg_scmap cg2))
                       <$> use ps_mines -- FIXME M.!

  mbpath <- pure $ cgAStar cg2 clust clust_mines

  return . maybe Pass Claim =<<
    case mbpath of
      Just (p1:p2:_) -> do
        case getRivers p1 p2 cg2 of
          (r:_) -> do
            debug "found astar line"
            return $ Just r
          _ -> return Nothing
      Nothing -> return Nothing


punter_main :: Maybe River -> UTCTime -> [Move] -> PunterState -> IO (Decision, PunterState)
punter_main randriver tstart moves ps@PunterState{..} =
  let
    (our_rivers, lost_rivers) = claimedRivers _ps_pid moves
    our_river = case our_rivers of
                  [r] -> Just r
                  [] -> Nothing

  in do
  flip runStateT ps $ do

    let de_random = maybe Pass Claim randriver

    debug $ "clasters_count " <> show (M.size (_cg_clusters _ps_cg))
    debug $ "scoremap_count " <> show (M.size (_cg_scoremaps _ps_cg))

    use ps_recovery >>= \r -> do
      when r $ do
        debug "FIXME recovery mode!"

    slowmode <- hasEnv "PUNTER_SLOW"
    when slowmode $ do
      slow <- Random.fromList [(True,1),(False,6)]
      when slow $ do
        debug "Stressmode: Entering fake slow path!"
        liftIO $ threadDelay (10*10^6)

    errmode <- hasEnv "PUNTER_ERROR"
    when errmode $ do
      err <- Random.fromList [(True,1),(False,6)]
      when err $ do
        error "Stressmode: nasty error from a list!"

    -- previous move accounting

    cg2 <- use ps_cg >>= \cg -> do
      pure $ flip execState cg $ do
        modify $ \cg -> maybe cg (mergeClustersByRiver cg) our_river
        forM_ lost_rivers $ \r -> do
          modify $ removeRiver r

    ps_enemy_rivers %= (map mkORiver lost_rivers ++)
    ps_cg %= const cg2

    -- new move strategies

    de_astar <- strategy_astar cg2
    diffTimeFrom tstart >>= \dt -> debug $ "time astar " <> show dt <> " decision " <> show de_astar

    de_option <- strategy_option cg2
    diffTimeFrom tstart >>= \dt -> debug $ "time option " <> show dt <> " decision " <> show de_option

    de_hotspot <- strategy_hotspot cg2
    diffTimeFrom tstart >>= \dt -> debug $ "time hotspot " <> show dt <> " decision " <> show de_option

    ps_recovery %= const False


    d <- chooseDecisionRnd [(de_astar, 7), (de_option, 1), (de_random, 1), (de_hotspot, 3)]

    case d of
      (Option r) -> do
        ps_enemy_rivers %= (delete $ mkORiver r)
        ps_num_options %= (\x -> x-1)
      _ -> return ()

    return d


punter :: UTCTime -> Gameplay_Req_S PunterState -> IO (Gameplay_Res_S PunterState)
punter tstart (Gameplay_Req_S moves ps@PunterState{..} ) = do
  mv <- newEmptyMVar

  rr <- force <$> randomRiver (view ps_cg ps)
  backup <- return (maybe Pass Claim rr, ps{ _ps_recovery = True} ) -- backup answer

  handle <- forkIO $ do
    catch ( do
        res <- force <$> punter_main rr tstart moves ps
        putMVar mv res
      )
      (\(e::SomeException) -> do
        debug $ "catched exception '" <> show e <> "', defaulting to " <> show (fst backup)
        putMVar mv backup
      )

  yield -- re-scheduling

  (mriv,ps2) <-
    flip execStateT backup $ do
      whileM $ do
        dt <- diffTimeFrom tstart
        case (dt > 0.8) of
          True -> do
            debug $ "aborting the execution due to timeout " <> show dt
            liftIO $ killThread handle
            return False -- timeout exit point
          False -> do
            mv <- liftIO $ tryTakeMVar mv
            case mv of
              Nothing -> do
                liftIO $ threadDelay (10^4)
                return True
              Just ret -> do
                put ret
                return False -- normal exit point

  case mriv of
    Claim river ->
      return (Gameplay_Res_S (claimRiver _ps_pid river) ps2 )
    Option river ->
      return (Gameplay_Res_S (optionRiver _ps_pid river) ps2 )
    Pass ->
      return (Gameplay_Res_S (movePass _ps_pid) ps2 )
