#!/bin/sh

if [ `whoami` == dvyal ]; then
    haskdogs --hasktags-args="-e -x" --use-stack=OFF
else
    haskdogs --use-stack=OFF
fi
